export const POSTS_GET = 'POSTS_GET';
export const POSTS_GET_SUCCESS = 'POSTS_GET_SUCCESS';
export const POSTS_GET_ERROR = 'POSTS_GET_ERROR';

export const postsGet = () => ({
	type: POSTS_GET,
});

export const postsGetSuccess = (data) => ({
	type: POSTS_GET_SUCCESS,
	data,
});

export const postsGetError = (error) => ({
	type: POSTS_GET_ERROR,
	error,
});
