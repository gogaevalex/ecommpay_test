import user from './userReducer';
import posts from './postsReducer';

const rootReducer = {
    user,
    posts,
};

export default rootReducer;
