import {
	POSTS_GET,
	POSTS_GET_SUCCESS,
	POSTS_GET_ERROR,
} from '@src/redux/actions';

const initialState = {
    isLoadPosts: false,
    dataPosts: null,
    error: null,
};

export default (state = initialState, action) => {
	switch (action.type) {
	case POSTS_GET:
		return {
			...state,
            isLoadPosts: true,
		};
	case POSTS_GET_SUCCESS:
		return {
			...state,
			dataPosts: action.data,
            isLoadPosts: false,
		};
	case POSTS_GET_ERROR:
		return {
			...state,
            isLoadPosts: false,
            error: action.error,
		};
	default:
		return state;
	}
};
