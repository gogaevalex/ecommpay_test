import {
	put, takeEvery, all, call, delay
} from 'redux-saga/effects';
import axios from 'axios';
import {
	POSTS_GET,
	postsGetSuccess,
	postsGetError,
} from '@src/redux/actions';

function* getPostsrControl() {
	const getPostsrRequest = () => axios({
		method: 'get',
		url: 'https://jsonplaceholder.typicode.com/posts',
	});
	try {
		const result = yield call(getPostsrRequest);
		yield delay(500)
		yield put(postsGetSuccess(result.data));
	} catch (error) {
		yield put(postsGetError(error.response));
	}
}

export default function* postsSaga() {
	yield all([
		takeEvery(POSTS_GET, getPostsrControl),
	]);
}
