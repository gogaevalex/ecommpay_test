import {fork, all} from 'redux-saga/effects';
import userSaga from './userSaga'
import postsSaga from './postsSaga'

export default function* root() {
	yield all([
		fork(userSaga),
		fork(postsSaga),
	]);
}
