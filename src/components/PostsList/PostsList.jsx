import {useState, useEffect} from 'react';
import st from 'styled-components';
import pt from 'prop-types';
import {Modal} from '@src/components/Modal';
import { ContentModalUser } from './components/ContentModalUser';
import { OneUserList } from './components/OneUserList';

export const PostsList = ({dataPosts}) => {
    const [resultData, setResultData] = useState({});
    const [activePost, setActivePost] = useState({
        body: '',
        isActive: false,
    });

    const [activeUser, setActiveUser] = useState({
        userId: null,
        isActive: false,
    });

    useEffect(() => {
        const data = {}

        dataPosts.forEach(({body, id, userId, title}, key) => {
            const currentAuthor = data[userId];
            const onePost = {
                id,
                title,
                body,
            };
    
            if (!currentAuthor) {
                data[userId] = [onePost]
            }
    
            if (currentAuthor) {
                data[userId].push(onePost);
            }

            if (dataPosts.length - 1 === key) {
                setResultData(data);
            }
        })
    }, [])

    const resultDataKeys = Object.keys(resultData);

    return (
        <Parent>
            {resultDataKeys.map((userId) =>  <OneUserList 
                key={userId}
                postsData={resultData[userId]}
                userId={userId} 
                openModalPost={setActivePost}
                openModalUser={setActiveUser} />
            )}

            {activePost.isActive && (
                <Modal onClickBg={() => setActivePost({body: '', isActive: false})}>
                    <ContentModalPost>{activePost.body}</ContentModalPost>
                </Modal>
            )}
            {activeUser.isActive && (
                <Modal onClickBg={() => setActiveUser({userId: null, isActive: false})}>
                    <ContentModalUser userId={activeUser.userId}/>
                </Modal>
            )}
        </Parent>
    );
}

PostsList.propTypes = {
    dataPosts: pt.array.isRequired,
};

const Parent = st.div`
    max-width: 500px;
    margin: auto;
`;

const ContentModalPost = st.div`
    font-size: ${({theme}) => theme.fontSize.h3};
`;