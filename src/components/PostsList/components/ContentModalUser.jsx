import {useEffect} from 'react';
import st from 'styled-components';
import pt from 'prop-types';
import {useDispatch, useSelector} from 'react-redux';
import {userGet} from '@redux/actions';
import {Skeleton} from '@src/components/Skeleton';

export const ContentModalUser = ({userId}) => {
    const dispatch = useDispatch();
	const {isLoadUser, dataUser} = useSelector((state) => state.user);
	useEffect(() => {
		dispatch(userGet({userId: userId}));
	}, []);
    if (!isLoadUser && dataUser) {
        const {name, email} = dataUser;
        return(
            <>
                <Name>{`name: ${name}`}</Name>
                <Mail>{`email: ${email}`}</Mail>
            </>
        )
    }
    return(
        <Skeleton width='300px' height='100px' margin='auto'/>
    )
}

ContentModalUser.propTypes = {
    userId: pt.string.isRequired,
};

const Name = st.div`
    margin: 0px 0px 20px 0px;
    font-size: ${({theme}) => theme.fontSize.h3};

`;

const Mail = st.div`
    font-size: ${({theme}) => theme.fontSize.h3};

`;