import st from 'styled-components';
import pt from 'prop-types';

export const OneUserList = ({postsData, userId, openModalPost, openModalUser}) => (
    <OneUser>
        <Author onClick={() => openModalUser({userId: userId, isActive: true})}>
            {`Author: ${userId}`}
        </Author>
        {postsData.map((currentPost) => (
            <OnePost 
                key={currentPost.title}
                onClick={() => openModalPost({body: currentPost.body, isActive: true})}
            >
                {currentPost.title}
            </OnePost>
        ))}    
    </OneUser>
);

OneUserList.propTypes = {
    postsData: pt.array.isRequired,
    userId: pt.string.isRequired,
    openModalPost: pt.func.isRequired,
    openModalUser: pt.func.isRequired,
};

const OneUser = st.div`
    margin: 40px 0px 0px 0px;
`;

const Author = st.div`
    box-sizing: border-box;
    font-size: ${({theme}) => theme.fontSize.h3};
    margin: 0px auto 20px auto;
    text-align: center;
    cursor: pointer;
    width: fit-content;
    outline: none;
    border-bottom: ${({theme}) => `1px solid ${theme.color.white}`};
    &:hover {
        border-bottom: ${({theme}) => `1px solid ${theme.color.dark.main}`};
    }
`;

const OnePost = st.div`
    font-size: ${({theme}) => theme.fontSize.h5};
    box-sizing: border-box;
    padding: 5px 20px;
    border: ${({theme}) => `1px solid ${theme.color.dark.main}`};
    border-radius: 20px;
    margin: 0px 0px 10px 0px;
    min-height: 100px;
    cursor: pointer;
    &:hover {
        background: ${({theme}) => theme.color.dark.bgHover};
    }
`;