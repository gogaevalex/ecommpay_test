import {useEffect} from 'react';
import st from 'styled-components';
import {useDispatch, useSelector} from 'react-redux';
import {postsGet} from '@redux/actions';
import {PostsList} from '@src/components/PostsList';
import {Skeleton} from '@src/components/Skeleton';

export const Posts = () => {
    const dispatch = useDispatch();
	const {isLoadPosts, dataPosts} = useSelector((state) => state.posts);
	useEffect(() => {
		dispatch(postsGet());
	}, []);
	return (
		<Parent>
			<Title>All posts</Title>
			{isLoadPosts && (
				<LoaderBlock>
					<Skeleton width='100%' height='2000px'/>
				</LoaderBlock>
			)}
			{!isLoadPosts && dataPosts && (
				<PostsList dataPosts={dataPosts ? dataPosts : []}/>	
			)}
		</Parent>
	);
};

const Parent = st.div`

`;

const Title = st.h1`
	margin: 0px;
	font-size: ${({theme}) => theme.fontSize.h1};
`;

const LoaderBlock = st.div`
	max-width: 500px;
	width: 100%;
	margin: auto;
`;