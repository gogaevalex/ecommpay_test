import {
	Route,
	Switch,
} from 'react-router-dom';
import {Posts} from '@src/pages/Posts';
import {MainLayout} from '@src/layouts/MainLayout';

const RootRouter = () => (
    <MainLayout>
        <Switch>
            <Route path={'/'} component={Posts} exact/>
        </Switch>
    </MainLayout>
);

RootRouter.propTypes = {
};

export default RootRouter;
